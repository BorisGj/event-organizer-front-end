import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(public httpClient: HttpClient) { }
  
  getUsers(): Observable<Object> {
    return this.httpClient.get('http://localhost:5000/dapi/allUsers');
  }

  delUser(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/terminate/' + id.toString());
  }

  getCitys() {
    return this.httpClient.get('http://localhost:5000/dapi/allcity');
  }

  getTypes() {
    return this.httpClient.get('http://localhost:5000/dapi/alltype');
  }

  reserve(tables: number, user: string, event: number) {
    return this.httpClient.get('http://localhost:5000/dapi/reserve/' + tables + "/" + user + "/" + event);
  }

  getevents(user: string) {
    return this.httpClient.get('http://localhost:5000/dapi/getevents/' + user);
  }

  getEventReservations() {
    return this.httpClient.get('http://localhost:5000/dapi/getresev/'); 
  }

  getUserReservations(usr: string) {
    return this.httpClient.get('http://localhost:5000/dapi/getresus/' + usr); 
  }

  cancelReservation(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/cancelres/' + id); 
  }

  delevent(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/delevent/' + id);
  }

  createAcc(obj: Object) {
    return this.httpClient.post('http://localhost:5000/dapi/createAcc', obj);
  }

  login(cred: string, psw: string) {
    return this.httpClient.get('http://localhost:5000/dapi/login/' + cred + "/" + psw);
  }

  updateAcc(id: number, user: string, eml: string, psw: string) {
    return this.httpClient.get('http://localhost:5000/dapi/updateAcc/' + id + "/" + user + "/" + eml + "/" + psw);
  }

  getcurrentUser(cred:string) {
    return this.httpClient.get('http://localhost:5000/dapi/currentUser/' + cred);
  }

  updateEvent(obj: Object) {
    return this.httpClient.post('http://localhost:5000/dapi/updatevent/', obj); 
  }

  report(rep: string, user: string, event: string) {
    return this.httpClient.get('http://localhost:5000/dapi/report/' + rep + "/" + user + "/" + event);
  }

  countevs(usr:string) {
    return this.httpClient.get('http://localhost:5000/dapi/countevs/' + usr);
  }

  countrevs(usr:string) {
    return this.httpClient.get('http://localhost:5000/dapi/countrevs/' + usr);
  }

  upload(blob: Object) {
    return this.httpClient.get('http://localhost:5000/dapi/upload/' + blob);
  }

  testadd(blob: Object) {
    return this.httpClient.post('http://localhost:5000/dapi/addevent', blob);
  }

  allevs() {
    return this.httpClient.get('http://localhost:5000/dapi/geteventsall');
  }

  get1event(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/getevent/' + id);
  }

  getreps() {
    return this.httpClient.get('http://localhost:5000/dapi/getreps');
  }

  getusers() {
    return this.httpClient.get('http://localhost:5000/dapi/getusers');
  }

  promote(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/promote/' + id);
  }

  demote(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/demote/' + id);
  }

  dismiss(id: number) {
    return this.httpClient.get('http://localhost:5000/dapi/dismissrep/' + id);
  }
}
