import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { EventModel } from 'src/app/models/eventModel';
import { Reservation } from 'src/app/models/reservationModel';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.page.html',
  styleUrls: ['./reservations.page.scss'],
})
export class ReservationsPage implements OnInit {
  user: string;
  theme: string;
  revs: Array<Reservation> = [];
  events: Array<EventModel> = [];

  constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private dom: DomSanitizer) { }

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
  }

  ionViewDidEnter(): void {
    this.getrevs();
    this.loadEvents();
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  themeopp() {
    if (this.theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  getrevs() {
    this.appService.getUserReservations(this.user).subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let key in response) {
          let obj: Reservation = {
            id: response[key].id,
            confirmationId: response[key].confirmationId,
            tables: response[key].tables,
            price: response[key].price,
            user: response[key].user,
            eventId: response[key].event
          }
          this.revs[i++] = obj;
        }
        console.log(this.revs);
      }
    )
  }

  loadEvents() {
    this.appService.allevs().subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let res in response) {
          const today = new Date().toISOString();
          if (response[res].startTime.localeCompare(new Date().toISOString()) == -1) {
            console.log("call it");
            this.appService.delevent(response[res].id).subscribe(
              response => {
                console.log(response);
                location.reload();
              }, error => {
                console.log(error);
                alert("Something went wrong. Older events will dispaly too, be careful.")
              }
            )
          }
        }
        for (let res in response) {
          let obj : EventModel = {
            id: response[res].id,
            eventName: response[res].eventName,
            eventDesc: response[res].eventDesc,
            url: this.dom.bypassSecurityTrustUrl(response[res].url),
            city: response[res].city,
            type: response[res].type,
            startTime: response[res].startTime,
            status: response[res].status,
            price: response[res].price,
            numTables: response[res].numTables,
            resTables: response[res].resTables,
            creator: response[res].creator
          }
          this.events[i++] = obj;
        }
        console.log(this.events);
      }, error => {
        console.log(error);
      }
    )
  }

  getevN(id: number) {
    let name: string;
    for (let i = 0; i < this.events.length; i++) {
      if (this.events[i].id == id) {
        name = this.events[i].eventName;
        break;
      }    
    }
    return name;
  }

  getevD(id: number) {
    let city: string;
    let date: string;
    for (let i = 0; i < this.events.length; i++) {
      if (this.events[i].id == id) {
        city = this.events[i].city;
        date = this.events[i].startTime;
        break;
      }    
    }
    return this.date(date) + ", in " + city;
  }

  date(event: string) {
    let y = "", d = "", m = "", h = "";
    y = event[0] + event[1] + event[2] + event[3];
    m = event[5] + event[6];
    d = event[8] + event[9];
    h = event[11] + event[12] + event[13] + event[14] + event[15];
    return d + "-" + m + "-" + y + " " + h;
  }

  download(res: Reservation) {
    let text = "Confirmation: " + res.confirmationId + ", Number of tickets: " + res.tables + ", Total price:  " + res.price + " DEN";  
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', String(res.confirmationId).split('.')[1]);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  cancelrev(id: number) {
    this.appService.cancelReservation(id).subscribe(
      response => {
        if (response["message"] == "Success") {
          alert("Successfully cancelled!");
          location.reload();
        } else {
          console.log(response);
          alert("Something went wrong, please try again later");
        }
      }, error => {
        console.log(error);
        alert("Something went wrong, please try again later");
      }
    )
  }

}
