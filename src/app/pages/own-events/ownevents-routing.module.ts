import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OwnEventsPage } from './ownevents.page';

const routes: Routes = [
  {
    path: '',
    component: OwnEventsPage
  },
  {
    path: 'add-event/:route',
    loadChildren: () => import('../add-event/add-event.module').then( m => m.AddEventPageModule)
  },
  {
    path: 'one-event/:action/:evid/:url',
    loadChildren: () => import('../events/one-event/one-event.module').then( m => m.OneEventPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OwnEventsPageRoutingModule {}
