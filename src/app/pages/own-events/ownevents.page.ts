import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { City } from 'src/app/models/cityModel';
import { EventModel } from 'src/app/models/eventModel';
import { Reservation } from 'src/app/models/reservationModel';
import { EventType } from 'src/app/models/typeModel';

@Component({
  selector: 'app-ownevents',
  templateUrl: './ownevents.page.html',
  styleUrls: ['./ownevents.page.scss'],
})
export class OwnEventsPage implements OnInit {

  user: string;
  theme: string;
  events: Array<EventModel> = [];
  revents: Array<EventModel> = [];
  cropevs: Array<EventModel> = [];
  tmpevents: Array<EventModel> = [];
  addeventUrl: string = "add-event/ownevents";
  tmprevs: Array<EventModel> = [];
  ordic: string = "arrow-up-outline"; ord: string = "Upcoming events first";
  str: string;
  type: string; types: Array<EventType> = [];
  city: string; citys: Array<City> = [];
  filtsh: boolean; txt: string = "use filter"; iconfilt: string = "color-filter-outline";
  showres: boolean = false;
  restext: string = "Show reservations"; resicon = "book-outline";
  evsel: string;
  revs: Array<Reservation> = []; ids: Array<number> = []; temprevs: Array<Reservation> = [];
  filtsel: boolean = false; txt2: string = "use filter"; iconfilt2: string = "color-filter-outline";

  constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private dom: DomSanitizer) { }

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
  }

  ionViewDidEnter(): void {
    this.loadEvents();
    this.loadCitys();
    this.loadTypes();
  }

  seeres() {
    if (this.showres == false) {
      this.showres = true;
      this.restext = "back to events";
      this.resicon = "beer-outline";
    } else {
      this.showres = false;
      this.restext = "show reservations";
      this.resicon = "book-outline";
    }
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }

  loadTypes() {
    this.appService.getTypes().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : EventType = {
            id: response[res].id,
            nameType: response[res].nameType,
            numEvents: response[res].numEvents
          }
          this.types[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  download(res: Reservation) {
    let text = "Confirmation: " + res.confirmationId + ", Number of tickets: " + res.tables + ", Total price:  " + res.price + " DEN";  
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', String(res.confirmationId).split('.')[1]);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  getevN(id: number) {
    let name: string;
    for (let i = 0; i < this.events.length; i++) {
      if (this.events[i].id == id) {
        name = this.events[i].eventName;
        break;
      }    
    }
    return name;
  }

  getevD(id: number) {
    let city: string;
    let date: string;
    for (let i = 0; i < this.events.length; i++) {
      if (this.events[i].id == id) {
        city = this.events[i].city;
        date = this.events[i].startTime;
        break;
      }    
    }
    return this.dates(date) + ", in " + city;
  }

  dates(event: string) {
    let y = "", d = "", m = "", h = "";
    y = event[0] + event[1] + event[2] + event[3];
    m = event[5] + event[6];
    d = event[8] + event[9];
    h = event[11] + event[12] + event[13] + event[14] + event[15];
    return d + "-" + m + "-" + y + " " + h;
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  cancelrev(id: number) {
    this.appService.cancelReservation(id).subscribe(
      response => {
        if (response["message"] == "Success") {
          alert("Successfully cancelled!");
          location.reload();
        } else {
          console.log(response);
          alert("Something went wrong, please try again later");
        }
      }, error => {
        console.log(error);
        alert("Something went wrong, please try again later");
      }
    )
  }

  loadEvents() {
    this.appService.getevents(this.user).subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let res in response) {
          const today = new Date().toISOString();
          if (response[res].startTime.localeCompare(new Date().toISOString()) == -1) {
            console.log("call it");
            this.appService.delevent(response[res].id).subscribe(
              response => {
                console.log(response);
                location.reload();
              }, error => {
                console.log(error);
                alert("Something went wrong. Older events will dispaly too, be careful.")
              }
            )
          }
        }
        for (let res in response) {
          let obj : EventModel = {
            id: response[res].id,
            eventName: response[res].eventName,
            eventDesc: response[res].eventDesc,
            url: this.dom.bypassSecurityTrustUrl(response[res].url),
            city: response[res].city,
            type: response[res].type,
            startTime: response[res].startTime,
            status: response[res].status,
            price: response[res].price,
            numTables: response[res].numTables,
            resTables: response[res].resTables,
            creator: response[res].creator
          }
          this.events[i] = obj;
          this.ids[i] = this.events[i].id;
          i++;
        }
        this.tmpevents = this.events;
        this.cropevs = this.events;
        let j = 0;
        for (let i = this.events.length - 1; i >= 0; i--) {
          this.revents[j++] = this.events[i];
        }
        this.tmprevs = this.revents;
        this.getrevs();
        console.log(this.events);
      }, error => {
        console.log(error);
      }
    )
  }

  themeopp() {
    if (this.theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  getrevs() {
    this.appService.getEventReservations().subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let key in response) {
          let obj: Reservation = {
            id: response[key].id,
            confirmationId: response[key].confirmationId,
            tables: response[key].tables,
            price: response[key].price,
            user: response[key].user,
            eventId: response[key].event
          }
          this.revs[i++] = obj;
        }
        this.revs = this.revs.filter(rev => this.ids.includes(rev.eventId));
        console.log(this.revs);
        this.temprevs = this.revs;
      }
    )
  }

  applyFilter() {
    if (this.city == undefined && this.type == undefined) {
      this.city = "All"; this.type = "Any";
    } else if (this.city == undefined) {
      this.city = "All";
    } else if (this.type == undefined) {
      this.type = "Any";
    } 

    
      if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents;
        this.cropevs = this.events;
        this.revents = this.tmprevs;
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type));
        this.revents = this.tmprevs.filter(event => (event.type == this.type));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
    } 
    
    if (this.str !== undefined) {
      this.modelChange();
    }
  }

   public modelChange(): void {
    let str = this.str.toLowerCase();
    if (str !== "" && str !== " " && str !== "  " && str !== "   " && str !== "    " && str !== "     ") {
      if (this.city !== undefined && this.type !== undefined) {
        if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
        this.revents = this.tmprevs.filter(event => event.eventName.toLowerCase().includes(str));
        this.cropevs = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
      } 
      } else {
        this.events = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
        this.revents = this.tmprevs.filter(event => event.eventName.toLowerCase().includes(str));
        this.cropevs = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
      }
    } else {
      if (this.city !== undefined && this.type !== undefined) {
        if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents;
        this.cropevs = this.events;
        this.revents = this.tmprevs;
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type));
        this.revents = this.tmprevs.filter(event => (event.type == this.type));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
      } 
      } else {
        this.events = this.tmpevents;
        this.revents = this.tmprevs;
        this.cropevs = this.tmpevents;     
      }
    }
    // Add code for searching here
   }
  
    cancelev(id: number) {
    this.appService.delevent(id).subscribe(
      response => {
        console.log(response);
        if (response["message"] == "Success") {
          alert("Event cancelled");
          location.reload();
        }
        else {
          alert(response["message"]);
        }
      },
      error => {
        console.log(error);
        alert("Something went wrong, please try again later!")
      }
    )
  }
  
  getRoute(eventid: number, goto: string) {
    return 'one-event/' + goto + "/" + String(eventid) + "/ownevents";
  }

  getDetails(string: string) {
    let limit = 100;
    let desc = "";
    if (string.length > limit) {
      for (let i = 0; i < limit; i++) {
        desc += string[i];
      }
    } else {
      desc = string;
    }
    return desc + " ...";
  }

  resDetails(event: EventModel) {
    if (event.price == null) {
      return false;
    } else {
      return true;
    }
  }

  loadCitys() {
    this.appService.getCitys().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : City = {
            id: response[res].id,
            nameCity: response[res].nameCity,
            numEvents: response[res].numEvents
          }
          this.citys[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  isUser(user: string) {
    if (this.user == user) {
      return true;
    } else {
      return false;
    }
  }

  filts() {
    if (this.filtsh) {
      this.filtsh = false;
      this.txt = "Use filter";
      this.iconfilt = "color-filter-outline";
      this.str = "";
      this.type = undefined;
      this.city = undefined;
      this.modelChange();
      this.applyFilter();
    } else {
      this.filtsh = true;
      this.txt = "Close Filter";
      this.iconfilt = "close-outline";
    }
  }

  filts2() {
    if (this.filtsel == false) {
      this.filtsel = true;
      this.txt2 = "Close Filter";
      this.iconfilt2 = "close-outline";
    } else {
      this.filtsel = false;
      this.txt2 = "Use filter";
      this.iconfilt2 = "color-filter-outline";
      this.evsel = undefined;
      this.applyFilterSel();
    }
  }

  applyFilterSel() {
    if (this.evsel == undefined) {
      this.revs = this.temprevs;
    } else {
      let id: number;
      for (let ev of this.events) {
        if (this.evsel == ev.eventName) {
          id = ev.id;
          break;
        }
      }
      this.revs = this.temprevs.filter(rev => rev.eventId == id);
    }
  }

  color(theme: string) {
    if (theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  order() {
    if (this.ordic == "arrow-up-outline") {
      this.ordic = "arrow-down-outline";
      this.ord = "Further Events First";
      this.events = this.revents;
    } else {
      this.ordic = "arrow-up-outline";
      this.ord = "Upcoming Events First"
      this.events = this.cropevs;
    }
  }

  date(event: EventModel) {
    let y = "", d = "", m = "", h = "";
    y = event.startTime[0] + event.startTime[1] + event.startTime[2] + event.startTime[3];
    m = event.startTime[5] + event.startTime[6];
    d = event.startTime[8] + event.startTime[9];
    h = event.startTime[11] + event.startTime[12] + event.startTime[13] + event.startTime[14] + event.startTime[15];
    return d + "-" + m + "-" + y + " " + h;
  }
}
