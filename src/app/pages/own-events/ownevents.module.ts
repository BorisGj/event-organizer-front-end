import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OwnEventsPageRoutingModule } from './ownevents-routing.module';

import { OwnEventsPage } from './ownevents.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwnEventsPageRoutingModule
  ],
  declarations: [OwnEventsPage]
})
export class OwnEventsPageModule {}
