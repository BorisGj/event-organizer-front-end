import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';
import { AppService } from 'src/app/app.service';
import { City } from 'src/app/models/cityModel';
import { EventModel } from 'src/app/models/eventModel';
import { Report } from 'src/app/models/reportModel';
import { User } from 'src/app/models/userModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


  constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private dom: DomSanitizer) { }


  citys: Array<City> = [];
  val = null;
  user: string;
  theme: string;
  events: Array<EventModel> = [];
  ownevents: string = "ownevents"; eventsurl: string = "events"; report: string = "report";
  status: string;
  tmpusers: Array<User> = [];
  users: Array<User> = [];
  reps: Array<Report> = [];
  reptext: string = "see reports"; repcon: string = "warning-outline"; repview: boolean = false;
  confirm: boolean = false;
  idterm: number;
  

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
  }

  ionViewDidEnter(): void {
    this.loadUsers();
    this.loadReps();
  }

  loadUsers() {
    this.appService.getusers().subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let key in response) {
          if (response[key].username == this.user) {
            this.status = response[key].type;
          }
          let obj: User = {
            id: response[key].id,
            username: response[key].username,
            email: response[key].email,
            type: response[key].type
          }
          this.tmpusers[i++] = obj;
        }
        this.users = this.tmpusers.filter(user => user.username != this.user);
      }, error => {
        console.log(error);
        alert("Warning, user type has not been loaded");
      }
    )
  }

  loadReps() {
    this.appService.getreps().subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let key in response) {
          let obj: Report = {
            id: response[key].id,
            report: response[key].report,
            userrep: response[key].userrep,
            eventrep: response[key].eventrep
          }
          this.reps[i++] = obj;
        }
      }
    )
  }


  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }


  logout() {
    sessionStorage.removeItem('user');
    window.location.replace("login");
  }

  relocate(url: string) {
    sessionStorage.setItem('theme', this.theme);
    sessionStorage.setItem('user', this.user);
    window.location.replace(url);
  }


  color(theme: string) {
    if (theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  reportnum(username: string) {
    let count = 0;
    for (let rep of this.reps) {
      if (rep.userrep == username) {
        count++;
      }
    } 
    return String(count) + "  Reports";
  } 

  gettype(type: string) {
    if (type == "usr") {
      return "User";
    } else {
      return "Admin";
    }
  }

  promote(id: number) {
    this.appService.promote(id).subscribe(
      response => {
        console.log(response);
        alert("Successfully promoted user to admin.");
        location.reload();
      }, error => {
        console.log(error);
        alert("Something went wrong. Please try again later.")
      }
    )
  }

  demote(id: number) {
    this.appService.demote(id).subscribe(
      response => {
        console.log(response);
        alert("Successfully demoted user to user.");
        location.reload();
      }, error => {
        console.log(error);
        alert("Something went wrong. Please try again later.")
      }
    )
  }

  themeopp() {
    if (this.theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  terminate(id: number) {
    this.confirm = true;
    this.idterm = id;
  }

  no() {
    this.confirm = false;
  }

  yes() {
    this.appService.delUser(this.idterm).subscribe(
      response => {
        console.log(response);
        alert("Successfully terminate account.");
        location.reload();
      }, error => {
        console.log(error);
        alert("Something went wrong. Please try again later.")
      }
    )
  }

  dismiss(id: number) {
    this.appService.dismiss(id).subscribe(
      response => {
        console.log(response);
        alert("Successfully dismissed report.");
        location.reload();
      }, error => {
        console.log(error);
        alert("Something went wrong. Please try again later.")
      }
    )
  }

  seereps() {
    if (this.repview) {
      this.repview = false;
      this.repcon = "warning-outline";
      this.reptext = "see reports";
    } else {
      this.repview = true;
      this.repcon = "person-outline";
      this.reptext = "back to accounts";
    }
  }
}
