import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'login',
    loadChildren: () => import('../folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'events',
    loadChildren: () => import('../events/events.module').then( m => m.EventsPageModule)
  },
  {
    path: 'ownevents',
    loadChildren: () => import('../own-events/ownevents.module').then( m => m.OwnEventsPageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('../suggestions/suggestions.module').then( m => m.SuggestionsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
