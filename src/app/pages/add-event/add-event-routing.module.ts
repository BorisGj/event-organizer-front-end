import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddEventPage } from './add-event.page';
import {NgxImageCompressService} from "ngx-image-compress";

const routes: Routes = [
  {
    path: '',
    component: AddEventPage
  },
  {
    path: 'events',
    loadChildren: () => import('../events/events.module').then( m => m.EventsPageModule)
  },
  {
    path: 'ownevents',
    loadChildren: () => import('../own-events/ownevents.module').then( m => m.OwnEventsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [NgxImageCompressService]
})
export class AddEventPageRoutingModule {}
