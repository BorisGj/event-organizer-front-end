import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { EventType } from 'src/app/models/typeModel';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { City } from 'src/app/models/cityModel';
import { CompressImageService } from './compress.service';
import { Injectable } from '@angular/core'
import { Observable, pipe} from 'rxjs'
import { map, filter, tap, take } from 'rxjs/operators'

// in bytes, compress images larger than 1MB
const fileSizeMax = 1 * 512 * 512;
// in pixels, compress images have the width or height larger than 1024px
const widthHeightMax = 600
const defaultWidthHeightRatio = 1
const defaultQualityRatio = 0.5


@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.page.html',
  styleUrls: ['./add-event.page.scss'],
})
export class AddEventPage implements OnInit {

  types: Array<EventType> = [];
  user: string;
  theme: string;
  urlBack: string;
  public formdata: FormGroup;
  base64: string;
  url: SafeUrl;
  url2: string;
  today: string;
  startDate: string;
  eventTitle: string;
  eventDesc: string;
  citys: Array<City> = [];
  city: string;
  type: string;
  ticketPrc: string;
  ticketNum: string;
  price: number;
  number: number;
  typefile: string;
  imgResultBeforeCompression: string = "";
  imgResultAfterCompression: string = "";
  form: any;
  typefileup: string;
 
  form2: any;
  


  constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private http: HttpClient,
  private dom: DomSanitizer, private compressImage: CompressImageService) { }

  ngOnInit() {
    this.urlBack = this.activatedRoute.snapshot.paramMap.get('route');
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
    this.formdata = new FormGroup({
      description: new FormControl(),
      family: new FormControl(),
      parent: new FormControl(),
      file: new FormControl(),
      fileSource: new FormControl()
    })
    this.form = document.getElementById("date");
    this.form.addEventListener("keypress", event => {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("block").click();
      }
    });
    this.form2 = document.getElementById("lastev");
    this.form2.addEventListener("keypress", event => {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("block").click();
      }
    });

    this.setUrl();

    this.today = new Date().toISOString();
  }

  ionViewDidEnter(): void {
    this.loadTypes();
    this.loadCitys();
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }

  setUrl() {
    if (sessionStorage.getItem('url') !== undefined) {
      this.url = this.dom.bypassSecurityTrustUrl(sessionStorage.getItem('url'));
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  getClassUrl() {
    if (this.url == undefined) {
      return "invis";
    } else {
      return "";
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  loadTypes() {
    this.appService.getTypes().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : EventType = {
            id: response[res].id,
            nameType: response[res].nameType,
            numEvents: response[res].numEvents
          }
          this.types[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  loadCitys() {
    this.appService.getCitys().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : City = {
            id: response[res].id,
            nameCity: response[res].nameCity,
            numEvents: response[res].numEvents
          }
          this.citys[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  onFileChange(event) {
    console.log(event);
    let file = event.target.files[0];
    this.typefileup = file.type;
    this.formdata.patchValue({
      fileSource: file
    })
    console.log(this.compress(file));
    this.uploadFile(file);
  }


  async uploadFile(file) {
    if (file) {
        var reader = new FileReader();

        reader.onload =this._handleReaderLoaded.bind(this);

        reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(event) {
    /*var binaryString = event.target.result;
    this.base64 = btoa(binaryString);
    this.url2 = "data:image/jpeg;base64, " + this.base64;
     const imgElement = document.createElement("img");
      imgElement.src = this.url2 as string;
      (document.querySelector("#imageu") as HTMLImageElement).src = this.url2 as string;

    imgElement.onload = this.crop.bind(this);*/
    console.log(event);
     var binaryString = event.target.result;
    this.base64 = btoa(binaryString);
    console.log(this.base64);
    this.url2 = "data:image/jpeg;base64, " + this.base64;
    sessionStorage.setItem('url', this.url2);
    this.url = this.dom.bypassSecurityTrustUrl("data:image/jpeg;base64, " + this.base64);
    var stringLength = this.base64.length - 'data:image/png;base64,'.length;

var sizeInBytes = 4 * Math.ceil((stringLength / 3))*0.5624896334383812;
    var sizeInKb = sizeInBytes / 1000;
    console.log(sizeInKb);
  }

  crop(e) {
    console.log(e.target);
      const canvas = document.createElement("canvas");
      const MAX_WIDTH = 600;
      let target = e.target as HTMLImageElement;
      const scaleSize = MAX_WIDTH / target.width;
      canvas.height = 400;
      canvas.width = 600;

      const ctx = canvas.getContext("2d");

      ctx.drawImage(target, 0, 0, canvas.width, canvas.height);
      let image = canvas.toDataURL("image/jpg");
    (document.querySelector("#output") as HTMLImageElement).src = image;
    this.url = image;
    sessionStorage.setItem('url', this.url as string);
    }

  geturl() {
    if (this.base64 == undefined) {
      return "place";
    } else {
      return "data:image/jpeg;base64, " + this.base64;
    }
    
  }

  addEvent() {
    if (this.ticketNum == undefined && this.ticketPrc !== undefined) {
      alert("Reservation details are optional. But please, add both price and number of tickets if you want to add any reservation details.");
    } else if (this.ticketNum !== undefined && this.ticketPrc == undefined) {
      alert("Reservation details are optional. But please, add both price and number of tickets if you want to add any reservation details.");
    } else {
      if (this.ticketNum !== undefined && this.ticketPrc == undefined) {
        this.price = null;
        this.number = null;
      } else {
        this.price = Number(this.ticketPrc);
        this.number = Number(this.ticketNum);
      }
      if (this.eventTitle !== undefined) {
        if (this.eventDesc !== undefined) {
          if (this.city !== undefined && this.type !== undefined) {
            if (this.startDate !== undefined) {
              if (this.url !== undefined && (this.typefileup == "image/jpg" || this.typefileup == "image/png" || this.typefileup == "image/jpeg")) {
                
                let obj = {
                  name: this.eventTitle,
                  date: this.startDate,
                  desc: this.eventDesc,
                  city: this.city,
                  type: this.type,
                  price: this.price,
                  tables: this.number,
                  blob: this.url,
                  creator: this.user
                }

                console.log(obj);

                this.appService.testadd(obj).subscribe(
                  response => {
                    console.log(response);
                    alert("Event successfully added. Press OK to continue.")
                    window.location.replace('events');
                  },
                  error => {
                    console.log(error);
                    alert("Uploaded image is too large.")
                  }
                )
                
              } else {
                alert("Image preview is required and it has to be .img, .jpeg or .jpg.");
              }
            } else {
              alert("starting date is required");
            }
          } else {
            alert("Selections for city and type are both required");
          }
        } else {
          alert("Description is required");
        }
      } else {
        alert("Title is required");
      }
    }
  }

  compress(file: File):void {
    
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = function (event) {
      console.log(event.target.result);
      const imgElement = document.createElement("img");
      imgElement.src = event.target.result as string;
      (document.querySelector("#imageu") as HTMLImageElement).src = event.target.result as string;

      imgElement.onload = function (e) {
        const canvas = document.createElement("canvas");
        const MAX_WIDTH = 600;
        let target = e.target as HTMLImageElement;
        const scaleSize = MAX_WIDTH / target.width;
        canvas.height = 400;
        canvas.width = 600;

        const ctx = <CanvasRenderingContext2D>canvas.getContext("2d");

        ctx.drawImage(target, 0, 0, canvas.width, canvas.height);
        let image = canvas.toDataURL("image/jpg");
        (document.querySelector("#output") as HTMLImageElement).src = image;
    
   
      } 

    }
  }

}

