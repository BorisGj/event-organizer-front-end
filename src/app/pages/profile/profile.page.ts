import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, UrlHandlingStrategy } from '@angular/router';
import { IonItem } from '@ionic/angular';
import { TouchSequence } from 'selenium-webdriver';
import { AppService } from 'src/app/app.service';
import { User } from 'src/app/models/userModel';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: string;
  theme: string;
  currentUser: User;
  numevs: number;
  numrevs: number;
  edit: boolean;
  updateUser: User = {
    id: null,
    username: null,
    email: null,
    password: null,
    type: null
  };
  change: any;
  strs: Array<string> = [];

  constructor(private activatedRoute: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
    this.edit = false;
    this.change = document.getElementById('table');
    this.change.addEventListener("keypress", event => {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("block").click();
      }
    });
  }

  color(val: string) {
    if (val=="type" || val=="id") {
      return "danger";
    } else {
      return "success";
    }
  }

  txt(val: string) {
    if (val == "type" || val=="id") {
      return "NE";
    } else {
      return "E";
    }
  }

  logout() {
    sessionStorage.removeItem('user');
    window.location.replace("login");
  }

  diss(val: string) {
    if (val == "type" || val=="id") {
      return true
    } else {
      return false
    }
  }

  tablecolor(theme: string, key: string) {
    let index: number;
    for (let i = 0; i < this.strs.length; i++) {
      if (this.strs[i] == key) {
        index = i;
        break;
      }
    }
    if (theme == "light") {
      if (index % 2 !== 0) {
        return theme;
      } else {
        return "secondary";
      }
    } else {
      if (index % 2 !== 0) {
        return theme;
      } else {
        return "tertiary";
      }
    }
  }

  ionViewDidEnter(): void {
    this.getCurrentUser();
    this.numEvsRevs();
  }

  numEvsRevs() {
    this.appService.countevs(this.user).subscribe(
      response => {
        console.log(response);
        this.numevs = response[0].count;
      },
      error => {
        console.log(error);
        alert("An error occured, please try again later.")
      }
    );
    this.appService.countrevs(this.user).subscribe(
      response => {
        console.log(response);
        this.numrevs = response[0].count;
      },
      error => {
        console.log(error);
        alert("An error occured, please try again later.")
      }
    );
  }

  getCurrentUser() {
    this.appService.getcurrentUser(this.user).subscribe(
      response => {
        console.log(response);
        let i = 0;;
        this.currentUser = {
          email: response[0].email,
          id: response[0].id,
          password: response[0].password,
          type: response[0].type,
          username: response[0].username,
        }
        for (let key in this.currentUser) {
          this.strs[i++] = key;
        }
        this.strs[i] = "nume";
        this.strs[i + 1] = "numr";
        console.log(this.strs);
      },
      error => {
        console.log(error);
        alert("Something went wrong. Please try again later");
      }
    )
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  firstletter(string:string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
 
  type(key: string) {
    if (key == "password") {
      return key;
    } else {
      return "text";
    }
  }

  holder(val: string, key: string) {
    if (key == "password") {
      return "********";
    } else {
      return val;
    }
  }

  updateacc() {
    let pswChange: boolean = false;
    if (this.updateUser["password"] !== null) {
      pswChange = true;
    }
    for (let key in this.updateUser) {
      if (this.updateUser[key] == null) {
        this.updateUser[key] = this.currentUser[key];
      }
    }
    console.log(this.updateUser);
    let format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    if (this.updateUser["email"] == "" || this.updateUser["username"] == "" || this.updateUser["password"] == "") {
      alert("All values are required");
    } else if (this.updateUser['email'].includes("@") == false || this.updateUser['email'].includes(".") == false) {
      alert("Invalid email");
    } else if (this.updateUser['password'].length < 6) {
      alert("Password is too weak. It has to be at least 6 characters long");
    } else if (/\d/.test(this.updateUser["password"]) == false) {
      alert("Please include some numbers in your password");
    } else if (format.test(this.updateUser["password"]) == false) {
      alert("Please include special characters in your password");
    } else {
      this.appService.updateAcc(this.updateUser["id"], this.updateUser["username"], this.updateUser["email"], this.updateUser["password"])
        .subscribe(
          response => {
            console.log(response);
            alert("Account succesfully updated");
            if (pswChange) {
              sessionStorage.removeItem('user');
              window.location.replace('login');
            } else {
              sessionStorage.setItem('user', this.updateUser["username"]);
              location.reload();
            }
          },
          error => {
            console.log(error);
            alert("Something went wrong. Please try again later.")
          }
      )
    }
  }

}
