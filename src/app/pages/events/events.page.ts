import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonItem } from '@ionic/angular';
import { AppService } from 'src/app/app.service';
import { EventType } from 'src/app/models/typeModel';
import { EventModel } from 'src/app/models/eventModel';
import { DomSanitizer, EventManager, SafeUrl } from '@angular/platform-browser';
import { City } from 'src/app/models/cityModel';
import { registerLocaleData } from '@angular/common';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  types: Array<EventType> = [];
  citys: Array<City> = [];
  user: string;
  theme: string;
  addeventUrl: string = "add-event/events";
  events: Array<EventModel> = [];
  tmpevents: Array<EventModel> = [];
  cropevs: Array<EventModel> = [];
  revents: Array<EventModel> = [];
  tmprevs: Array<EventModel> = [];
  city: string; type: string;
  txt: string = "Use Filter";
  filtsh: boolean = false;
  iconfilt: string = "color-filter-outline";
  str: string;
  ordic: string = "arrow-up-outline"; ord: string = "Upcoming events first";

  constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private dom: DomSanitizer) { }

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
  }

  ionViewDidEnter(): void {
    this.loadEvents();
    this.loadCitys();
    this.loadTypes();
  }

  date(event: EventModel) {
    let y = "", d = "", m = "", h = "";
    y = event.startTime[0] + event.startTime[1] + event.startTime[2] + event.startTime[3];
    m = event.startTime[5] + event.startTime[6];
    d = event.startTime[8] + event.startTime[9];
    h = event.startTime[11] + event.startTime[12] + event.startTime[13] + event.startTime[14] + event.startTime[15];
    return d + "-" + m + "-" + y + " " + h;
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }

  order() {
    if (this.ordic == "arrow-up-outline") {
      this.ordic = "arrow-down-outline";
      this.ord = "Further Events First";
      this.events = this.revents;
    } else {
      this.ordic = "arrow-up-outline";
      this.ord = "Upcoming Events First"
      this.events = this.cropevs;
    }
  }

  color(theme: string) {
    if (theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }

  filts() {
    if (this.filtsh) {
      this.filtsh = false;
      this.txt = "Use filter";
      this.iconfilt = "color-filter-outline";
      this.str = "";
      this.type = undefined;
      this.city = undefined;
      this.modelChange();
      this.applyFilter();
    } else {
      this.filtsh = true;
      this.txt = "Close Filter";
      this.iconfilt = "close-outline";
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  isUser(user: string) {
    if (this.user == user) {
      return true;
    } else {
      return false;
    }
  }

  loadTypes() {
    this.appService.getTypes().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : EventType = {
            id: response[res].id,
            nameType: response[res].nameType,
            numEvents: response[res].numEvents
          }
          this.types[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  loadCitys() {
    this.appService.getCitys().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : City = {
            id: response[res].id,
            nameCity: response[res].nameCity,
            numEvents: response[res].numEvents
          }
          this.citys[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  loadEvents() {
    this.appService.allevs().subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let res in response) {
          const today = new Date().toISOString();
          if (response[res].startTime.localeCompare(new Date().toISOString()) == -1) {
            console.log("call it");
            this.appService.delevent(response[res].id).subscribe(
              response => {
                console.log(response);
                location.reload();
              }, error => {
                console.log(error);
                alert("Something went wrong. Older events will dispaly too, be careful.")
              }
            )
          }
        }
        for (let res in response) {
          let obj : EventModel = {
            id: response[res].id,
            eventName: response[res].eventName,
            eventDesc: response[res].eventDesc,
            url: this.dom.bypassSecurityTrustUrl(response[res].url),
            city: response[res].city,
            type: response[res].type,
            startTime: response[res].startTime,
            status: response[res].status,
            price: response[res].price,
            numTables: response[res].numTables,
            resTables: response[res].resTables,
            creator: response[res].creator
          }
          this.events[i++] = obj;
        }
        this.tmpevents = this.events;
        this.cropevs = this.events;
        let j = 0;
        for (let i = this.events.length - 1; i >= 0; i--) {
          this.revents[j++] = this.events[i];
        }
        this.tmprevs = this.revents;
      }, error => {
        console.log(error);
      }
    )
  }

  public modelChange(): void {
    let str = this.str.toLowerCase();
    if (str !== "" && str !== " " && str !== "  " && str !== "   " && str !== "    " && str !== "     ") {
      if (this.city !== undefined && this.type !== undefined) {
        if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
        this.revents = this.tmprevs.filter(event => event.eventName.toLowerCase().includes(str));
        this.cropevs = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.eventName.toLowerCase().includes(str)));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city && event.eventName.toLowerCase().includes(str)));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city && event.eventName.toLowerCase().includes(str)));
      } 
      } else {
        this.events = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
        this.revents = this.tmprevs.filter(event => event.eventName.toLowerCase().includes(str));
        this.cropevs = this.tmpevents.filter(event => event.eventName.toLowerCase().includes(str));
      }
    } else {
      if (this.city !== undefined && this.type !== undefined) {
        if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents;
        this.cropevs = this.events;
        this.revents = this.tmprevs;
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type));
        this.revents = this.tmprevs.filter(event => (event.type == this.type));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
      } 
      } else {
        this.events = this.tmpevents;
        this.revents = this.tmprevs;
        this.cropevs = this.tmpevents;     
      }
    }
    // Add code for searching here
  }

  applyFilter() {
    if (this.city == undefined && this.type == undefined) {
      this.city = "All"; this.type = "Any";
    } else if (this.city == undefined) {
      this.city = "All";
    } else if (this.type == undefined) {
      this.type = "Any";
    } 

    
      if (this.city == "All" && this.type == "Any") {
        this.events = this.tmpevents;
        this.cropevs = this.events;
        this.revents = this.tmprevs;
      } else if (this.city == "All" && this.type !== "Any") {
        this.events = this.tmpevents.filter(event => (event.type == this.type));
        this.revents = this.tmprevs.filter(event => (event.type == this.type));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type));
      } else if (this.city !== "All" && this.type == "Any") {
        this.events = this.tmpevents.filter(event => (event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.city == this.city));
      } else {
        this.events = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
        this.revents = this.tmprevs.filter(event => (event.type == this.type && event.city == this.city));
        this.cropevs = this.tmpevents.filter(event => (event.type == this.type && event.city == this.city));
    } 
    
    if (this.str !== undefined) {
      this.modelChange();
    }
  }

  resDetails(event: EventModel) {
    if (event.price == null) {
      return false;
    } else {
      return true;
    }
  }

  checkSize() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor);
    return check;
  }

  getDetails(string: string) {
    let limit = 100;
    let desc = "";
    if (string.length > limit) {
      for (let i = 0; i < limit; i++) {
        desc += string[i];
      }
    } else {
      desc = string;
    }
    return desc + " ...";
  }

  cancelev(id: number) {
    this.appService.delevent(id).subscribe(
      response => {
        console.log(response);
        if (response["message"] == "Success") {
          alert("Event cancelled");
          location.reload();
        }
        else {
          alert(response["message"]);
        }
      },
      error => {
        console.log(error);
        alert("Something went wrong, please try again later!")
      }
    )
  }
  
  getRoute(eventid: number, goto: string) {
    return 'one-event/' + goto + "/" + String(eventid) + "/back";
  }

}
