import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OneEventPage } from './one-event.page';

const routes: Routes = [
  {
    path: '',
    component: OneEventPage
  },
  {
    path: 'back',
    loadChildren: () => import('../events.module').then( m => m.EventsPageModule)
  },
  {
    path: 'reserve',
    loadChildren: () => import('../../reservations/reservations.module').then( m => m.ReservationsPageModule)
  },
  {
    path: 'ownevents',
    loadChildren: () => import('../../own-events/ownevents.module').then( m => m.OwnEventsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OneEventPageRoutingModule {}
