import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OneEventPageRoutingModule } from './one-event-routing.module';

import { OneEventPage } from './one-event.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OneEventPageRoutingModule
  ],
  declarations: [OneEventPage]
})
export class OneEventPageModule {}
