import { Component, OnInit } from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { City } from 'src/app/models/cityModel';
import { EventModel } from 'src/app/models/eventModel';
import { EventType } from 'src/app/models/typeModel';


@Component({
  selector: 'app-one-event',
  templateUrl: './one-event.page.html',
  styleUrls: ['./one-event.page.scss'],
})
export class OneEventPage implements OnInit {


  constructor(private activatedRoute: ActivatedRoute, private appService: AppService,
  private dom: DomSanitizer) { }

  citys: Array<City> = [];
  types: Array<EventType> = [];
  val = null;
  toEdit: string;
  user: string;
  theme: string;
  idEv: number;
  edit: boolean;
  title: string;
  urlBack : string;
  reserve: boolean = false;
  event: Array<EventModel> = [];
  today: string;
  startDate: string;
  name: string;
  desc: string;
  cityEdit: string;
  typeEdit: string;
  tickets: number;
  price: number;
  status = "reserve";
  rtickets: number;
  iconre: string = "book-outline";
  resTickets: number = 1;
  reslink: string = 'reserve';


  ngOnInit() {
    this.urlBack = this.activatedRoute.snapshot.paramMap.get('url');
    this.toEdit = this.activatedRoute.snapshot.paramMap.get('action');
    this.idEv = Number(this.activatedRoute.snapshot.paramMap.get('evid'));
    console.log(this.idEv); console.log(this.toEdit);
    console.log(sessionStorage.getItem('user'));
    if (this.toEdit == "edit") {
      this.edit = false;
      this.title = "Edit ";
    } else if (this.toEdit == "view") {
      this.edit = true;
      this.title = "View "
    } else {
      this.reserve = true;
      this.title = "Reserve ";
    }
    console.log(this.edit);
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
    this.today = new Date().toISOString();
  }

  reswitch() {
    if (this.reserve) {
      this.reserve = false; 
      this.title = "View ";
      this.status = "reserve";
      this.iconre = "book-outline";
    } else {
      this.reserve = true;
      this.title = "Reserve ";
      this.status = "Back to view";
      this.iconre = "eye-outline";
    }
  }

  ionViewDidEnter(): void {
    this.loadEvent();
    this.loadCitys();
    this.loadTypes();
  }


  getTitle(str: string) {
    return str[0].toUpperCase() + str.slice(1);
  } 

  date(event: EventModel) {
    let y = "", d = "", m = "", h = "";
    y = event.startTime[0] + event.startTime[1] + event.startTime[2] + event.startTime[3];
    m = event.startTime[5] + event.startTime[6];
    d = event.startTime[8] + event.startTime[9];
    h = event.startTime[11] + event.startTime[12] + event.startTime[13] + event.startTime[14] + event.startTime[15];
    return d + "-" + m + "-" + y + " " + h;
  }

  avail(int1: number, int2: number) {
    return int1 - int2;
  }
  
  loadCitys() {
    this.appService.getCitys().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : City = {
            id: response[res].id,
            nameCity: response[res].nameCity,
            numEvents: response[res].numEvents
          }
          this.citys[i++] = obj;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  loadTypes() {
    this.appService.getTypes().subscribe(
      response => {
        let i = 0;
        for (let res in response) {
          let obj : EventType = {
            id: response[res].id,
            nameType: response[res].nameType,
            numEvents: response[res].numEvents
          }
          this.types[i++] = obj;
        }
        console.log(this.types);
      },
      error => {
        console.log(error);
      }
    )
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  logout() {
    sessionStorage.removeItem('user');
    window.location.replace("login");
  }

  relocate(url: string) {
    sessionStorage.setItem('theme', this.theme);
    sessionStorage.setItem('user', this.user);
    window.location.replace(url);
  }

  loadEvent() {
    this.appService.get1event(this.idEv).subscribe(
      response => {
        console.log(response);
        let i = 0;
        for (let res in response) {
          let obj : EventModel = {
            id: response[res].id,
            eventName: response[res].eventName,
            eventDesc: response[res].eventDesc,
            url: this.dom.bypassSecurityTrustUrl(response[res].url),
            city: response[res].city,
            type: response[res].type,
            startTime: response[res].startTime,
            status: response[res].status,
            price: response[res].price,
            numTables: response[res].numTables,
            resTables: response[res].resTables,
            creator: response[res].creator
          }
          this.event[i++] = obj;
        }
        this.name = this.event[0].eventName;
        this.desc = this.event[0].eventDesc;
        this.typeEdit = this.event[0].type;
        this.cityEdit = this.event[0].city;
        this.tickets = this.event[0].numTables;
        this.price = this.event[0].price;
        this.startDate = this.event[0].startTime;
        this.rtickets = this.event[0].resTables;
      }, error => {
        console.log(error);
      }
    )
  }

  updateE(id: number) {
    console.log(this.startDate);
    console.log(this.name);
    console.log(this.desc);
    console.log(this.cityEdit);
    console.log(this.tickets);
    console.log(this.price);
    console.log(this.typeEdit);
    let prc: number, tck: number;
    if (this.tickets == null && this.price !== null) {
      alert("Reservation details are optional. But please, add both price and number of tickets if you want to add any reservation details.");
    } else if (this.tickets!== null && this.price == null) {
      alert("Reservation details are optional. But please, add both price and number of tickets if you want to add any reservation details.");
    } else {
      if (this.name !== undefined) {
        if (this.desc !== undefined) {
          if (this.cityEdit !== undefined && this.typeEdit !== undefined) {
            if (this.startDate !== undefined) {
              let obj = {
                name: this.name,
                desc: this.desc,
                city: this.cityEdit,
                start: this.startDate,
                type: this.typeEdit,
                price: this.price,
                tickets: this.tickets,
                id: id
              }
                
              this.appService.updateEvent(obj)
                .subscribe(
                  response => {
                    if (response["input"] == "OK") {
                      alert("Successfully updated");
                      document.getElementById("back").click();
                    }
                    else {
                      console.log(response);
                      alert("Something went wrong. Try again later.");
                    }
                  }, error => {
                    console.log(error);
                    alert("Something went wrong. Try again later.");
                }
              )
              }
            } else {
              alert("starting date is required");
            }
          } else {
            alert("Selections for city and type are both required");
          }
        } else {
          alert("Description and title are required");
        }
      }
  }

  reserveT() {
    if (this.resTickets > this.avail(this.tickets, this.rtickets)) {
      alert("you can't reserve that many tickets");
    } else {
      if (this.resTickets <= 0) {
        alert("you can't reserve that number of tickets");
      } else {
        this.appService.reserve(this.resTickets, this.user, this.event[0].id).subscribe(
          response => {
            console.log(response);
            alert("successfully reserved");
            document.getElementById("reslink").click();
          }, error => {
            console.log(error);
            alert("Something went wrong, please try again later.")
          }
        )
      }
    }
  }

}
