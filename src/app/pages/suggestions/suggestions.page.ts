import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonItem } from '@ionic/angular';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.page.html',
  styleUrls: ['./suggestions.page.scss'],
})
export class SuggestionsPage implements OnInit {
  user: string;
  theme: string;
  report: string;
  revent: string;
  ruser: string;
  form: any;
  whyreport: string;
  whyreport2: string;
  whyreport3: string;
  constructor(private activatedRoute: ActivatedRoute, private appService: AppService) { }

  ngOnInit() {
    console.log(sessionStorage.getItem('user'));
    this.user = sessionStorage.getItem('user');
    if (this.user == undefined) {
      window.location.replace("login");
    }
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
    this.form = document.getElementById('form');
    this.form.addEventListener("keypress", event => {
      if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("subm").click();
      }
    });
    this.whyreport = 'We like to allow anyone to be able to host thier event and advertise in on this plaform. However, this will also ' +
      'allow people to use to platform to create fake events and spread false information.';
    this.whyreport2 = 'We have a team of admins that ' +
      'constantly checks for suspicious activity, but they cannot get everything. This is why it is important that you report ' +
      'anything suspicious that you notice. The more reports something has, the more attention it will get';
    this.whyreport3 = 'Do not worry about ' +
      'reporting an innocent event, all reports will first be investigated by an admin before deciding wether action should be taken or not. '
      + "You can add a report below";
  }

  ionViewDidEnter(): void {
  }

  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }

  opptheme(theme: string) {
    if (theme == "light") {
      return "secondary";
    } else {
      return "tertiary";
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  submitReport() {
    console.log(this.report);
    if (this.report == undefined || this.ruser == undefined || this.revent == undefined) {
      alert("All fields are required");
    } else {
      this.appService.report(this.report, this.ruser, this.revent).subscribe(
        response => {
          console.log(response);
          if (response["message"] == "success") {
            alert("Report successfully submitted. Thank you for your cooperation.");
            location.reload();
          } else {
            alert("An error occured. Please try again later.");
          }
        },
        error => {
          console.log(error);
          alert("An error occured. Please try again later.");
        }
      )
    }
  }

  btntheme(theme: string) {
    if (theme == "dark") {
      return "secondary";
    } else {
      return "primary";
    }
  }
}
