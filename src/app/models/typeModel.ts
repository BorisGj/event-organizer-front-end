export interface EventType {
    id: number,
    nameType: string,
    numEvents: number
}