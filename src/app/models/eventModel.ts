import { SafeUrl } from "@angular/platform-browser";

export interface EventModel {
    id: number,
    eventName: string,
    eventDesc: string,
    url: SafeUrl,
    city: string,
    type: string,
    startTime: string,
    status: number,
    price: number,
    numTables: number,
    resTables: number,
    creator: string
}