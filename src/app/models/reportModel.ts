export interface Report {
    id: number,
    report: string,
    userrep: string,
    eventrep: string;
}