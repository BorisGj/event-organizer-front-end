export interface Reservation {
    id: number,
    confirmationId: number,
    tables: number,
    price: number,
    user: string,
    eventId: number
}