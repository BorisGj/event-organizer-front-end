export interface City {
    id: number,
    nameCity: string,
    numEvents: number
}