import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  theme: string;
  user: string;
  status: string;
  ngOnInit(): void {
    if (sessionStorage.getItem('theme') == undefined) {
      sessionStorage.setItem('theme', "dark");
      this.theme = sessionStorage.getItem('theme');
    } else {
      this.theme = sessionStorage.getItem('theme');
    }
  }

  ionViewDidEnter(): void {
  }


  icon(theme: string) {
    if (theme == "light") {
      return "moon-outline";
    } else {
      return "sunny-outline"
    }
  }
  
  btnTxt(theme: string) {
    if (theme == "light") {
      return "Dark Mode";
    } else {
      return "Light Mode"
    }
  }

  themecng() {
    if (this.theme == "light") {
      this.theme = "dark";
      sessionStorage.setItem('theme', "dark");
    } else {
      this.theme = "light";
      sessionStorage.setItem('theme', "light");
    }
  }

  public toggle = false;
  public appPages = [
    { title: 'Home', url: 'home', icon: 'home' },
    { title: 'Events', url: 'events', icon: 'beer' },
    { title: 'Reservations', url: 'reservations', icon: 'book' },
    { title: 'Your Events', url: 'yourevs', icon: 'headset' },
    { title: 'Your Porfile', url: 'profile', icon: 'person-circle' },
    { title: 'Report', url: 'suggestions', icon: 'document-text' },
  ];
  public labels = ['Home', 'Events', 'Reservations', 'Your Events', 'Your Porfile', 'Report'];
  constructor() {}
}
